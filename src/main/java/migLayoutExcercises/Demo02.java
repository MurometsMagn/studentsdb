package migLayoutExcercises;
//https://www.youtube.com/watch?v=aKc6WzHPatY


import net.miginfocom.swing.MigLayout;
import org.w3c.dom.Text;

import javax.swing.*;
import java.awt.*;

public class Demo02 extends JFrame {

    public Demo02() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        setLayout(new MigLayout());

        JLabel title = new JLabel("Registration form", JLabel.CENTER);
        title.setOpaque(true);
        title.setBackground(Color.BLACK);
        title.setForeground(Color.WHITE);
        title.setPreferredSize(new Dimension(1, 27));

        add(title, "span, wrap 15, growx");
        add(new JLabel("Name"), "al right");
        add(createTextTield(), "wrap, pushx, growx");
        add(new JLabel("Phone"), "al right");
        add(createTextTield(), "wrap, pushx, growx");
        add(new JLabel("DOB"), "al right");
        add(new JComboBox(new String[]{"1989"}), "split 3");
        add(new JComboBox(new String[]{"01"}));
        add(new JComboBox(new String[]{"01"}), "wrap");
        //add(new JComboBox<>(new String("1998")));
        add(new JLabel("Address"), "al right");
        add(new JScrollPane(new JTextArea(3, 10)), "wrap, pushx, growx");
        add(new JLabel("Resume"), "al right");
        add(createTextTield(), "pushx, growx, split 2");
        add(new JButton("..."), "wrap 10");
        add(new JLabel());
        add(new JButton("Register"));

        setPreferredSize(new Dimension(350, 300));

        pack();
        setVisible(true);
    }

    //задает высоту:
    private JTextField createTextTield() {
        JTextField tf = new JTextField();
        tf.setPreferredSize(new Dimension(1, 27));
        return tf;
    }

    public static void main(String[] args) {
        new Demo02();
    }
}
