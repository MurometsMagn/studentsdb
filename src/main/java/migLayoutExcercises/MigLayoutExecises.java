package migLayoutExcercises;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

            /* jFrame
            jPanel
            fill jPanel
            add jPanel to jFrame
            setVisible
             */

public class MigLayoutExecises{
    public static void main(String[] args) {
        try {
            new StartFrame();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
//    private static void initGUI(JPanel jPanel, String title) {
//        //порядок не менять:
//        setTitle(title);
//        setSize(800, 600);
//        setMinimumSize(new Dimension(300, 200));
//        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //can be replaced
//        setLocationRelativeTo(null);
//        setVisible(true);
//    }
}

class MainMenuPage extends JPanel{
    //JPanel panel = new JPanel(new MigLayout());
    JLabel firstNameLabel = new JLabel("First Name");
    JTextField firstNameTextField = new JTextField();
    JLabel lastNameLabel = new JLabel("Last Name");
    JTextField lastNameTextField = new JTextField();
    JLabel addressLabel = new JLabel("Address");
    JTextField addressTextField = new JTextField();

    public MainMenuPage() {
        super(new MigLayout());
        create();
    }
    //MigLayout migLayout = new MigLayout();

    void create() {
        add(firstNameLabel);
        add(firstNameTextField);
        add(lastNameLabel, "gap unrelated");
        add(lastNameTextField, "wrap"); //wrap to next row
        add(addressLabel);
        add(addressTextField, "span, grow");
    }
}

class StartFrame extends JFrame {

    public StartFrame() {
        this(new MainMenuPage());
    }

    public StartFrame(JPanel jPanel) {
        //порядок не менять:
        super("MigLayoutExecises");
        setSize(800, 600);
        setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //can be replaced
        setLocationRelativeTo(null);
        //setLayout(new MigLayout());
        //add(jPanel, "dock center");
        add(jPanel);
        //getContentPane().add(jPanel);
        setVisible(true);
    }
}



