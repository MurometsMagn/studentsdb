package migLayoutExcercises;
//https://www.youtube.com/watch?v=sLIveQh2ZUg

import net.miginfocom.swing.MigLayout;

import javax.swing.*;

//     column1          +    column2
//                      |
//+---------------------|---------------------------------------------------------+
//|                     |                                                         |
//|---------------------|---------------------------------------------------------|
//|                     |                                                         |
//|                     |  +--------------------------------------------------+   |
//|    First name:      |  |                                                  |   |
//|                     |  +--------------------------------------------------+   |    row 1
//|---------------------|---------------------------------------------------------------------+
//|                     | +--------------------------------------------------+    |
//|    Family name:     | |                                                  |    |
//|                     | +--------------------------------------------------+    |    row 2
//|---------------------|---------------------------------------------------------------------+
//|    Details:         |                                                         |
//|    +----------------|-----------------------------------------------------+   |
//|    |                |                                                     |   |
//|    |                |                                                     |   |
//|    |                |                                                     |   |
//|    |                |                                                     |   |
//|    |                |                                                     |   |
//|    |                |                                                     |   |
//|    +----------------------------------------------------------------------+   |    row 3
//+-------------------------------------------------------------------------------+





public class SimpleMig1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("This is a frame");
        frame.setSize(200, 200);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("GUI #1"));
        frame.add(panel);

        panel.setLayout(new MigLayout("debug","[]10[]", "[] [] []")); //twu colums and three rows

        JLabel firstNameLabel = new JLabel("First name:");
        JLabel familyNameLabel = new JLabel("Family name:");
        JTextField firstName = new JTextField(20);
        JTextField familyName = new JTextField(20);
        JLabel details = new JLabel("Details:");
        JTextArea detailsArea = new JTextArea(30, 20);
        detailsArea.setBorder(BorderFactory.createEtchedBorder());

        panel.add(firstNameLabel, "left, sg 1, split2");
        panel.add(firstName, "pushx, growx, wrap");

        panel.add(familyNameLabel, "left, sg 1, split2");
        panel.add(familyName, "pushx, growx, wrap");

        panel.add(details, "left, wrap");
        panel.add(detailsArea, "span2, push, grow, wrap");

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
