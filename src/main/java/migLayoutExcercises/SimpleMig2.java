package migLayoutExcercises;
//https://www.youtube.com/watch?v=hWtiSRnXz3w&t=30s

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class SimpleMig2 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("This is a frame");
        frame.setSize(400, 400);
        frame.setMinimumSize(new Dimension(400, 400));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("GUI #1"));
        frame.add(panel);

        JLabel firstNameLabel = new JLabel("First name:");
        JTextField firstName = new JTextField();
        JLabel familyNameLabel = new JLabel("Family name:");
        JTextField familyName = new JTextField();
        JLabel dataLabel = new JLabel("Date:");
        JTextField description = new JTextField();
        JButton calendar = new JButton("Calendar");

        //row 4 has 2  elements:
        JLabel addressLabel = new JLabel("Address: ");
        JTextField address = new JTextField();

        //row 5 has 4 elements:
        JLabel mobilePhoneLabel = new JLabel("MobilePhone: ");
        JTextField mobile = new JTextField();
        JCheckBox textCheck = new JCheckBox();
        textCheck.setEnabled(true);
        JCheckBox emailCheck = new JCheckBox();
        emailCheck.setEnabled(true);

        //row 6 has 2 elements:
        JLabel additionalLabel = new JLabel("Additional info:");
        JTextArea additionalArea = new JTextArea(30, 20);
        additionalArea.setBorder(BorderFactory.createEtchedBorder());

        panel.setLayout(new MigLayout("", "", ""));
        //row 1
        panel.add(firstNameLabel, "split 2, sg a");
        panel.add(firstName, "pushx, growx, wrap");
        //row 2
        panel.add(familyNameLabel, "split 2, sg a");
        panel.add(familyName, "pushx, growx, wrap");
        //row 3
        panel.add(dataLabel, "split 3, sg a");
        panel.add(description, "pushx, growx");
        panel.add(calendar, "wrap");
        //row 4
        panel.add(addressLabel, "split 2, sg a");
        panel.add(address, "pushx, growx, wrap");
        //row 5
        panel.add(mobilePhoneLabel, "split 6, sg a");
        panel.add(mobile, "pushx, growx");
        panel.add(new JLabel("text:"), "");
        panel.add(textCheck);
        panel.add(new JLabel("email:"), "");
        panel.add(emailCheck, "wrap");
        //row 6
        panel.add(additionalLabel, "wrap, sg a");
        panel.add(additionalArea, "span, push, grow");

        frame.pack();
        frame.setLocation(300, 300);
        //frame.setSize(new Dimension(400, 500));
        frame.setVisible(true);
    }
}
