package migLayoutExcercises.mediator;

public class Mediator {
    private MyPanel panel = new MyPanel();
    private MyButton button = new MyButton();

    public Mediator() {
        panel.addListener(new MyPanel.MyActionListener() {
            @Override
            public void actionPerformed() {
                button.setTitle("Что-то произошло в панели");
            }
        });
    }

    public String getButtonTitle() {
        return button.getTitle();
    }

    public void triggerEvent() {
        panel.event();
    }

    public static void main(String[] args) {
        Mediator window = new Mediator();
        System.out.println(window.getButtonTitle());
        window.triggerEvent();
        System.out.println(window.getButtonTitle());
    }
}
//splitter