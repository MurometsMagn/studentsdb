package migLayoutExcercises.mediator;

public class MyButton {
    private String title = "Кнопка";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
