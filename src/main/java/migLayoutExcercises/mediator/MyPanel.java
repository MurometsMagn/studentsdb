package migLayoutExcercises.mediator;

import java.util.ArrayList;
import java.util.List;

public class MyPanel {
    public interface MyActionListener {
        void actionPerformed();
    }

    private List<MyActionListener> listeners = new ArrayList<>();

    public void addListener(MyActionListener listener) {
        listeners.add(listener);
    }

    private void notifyListeners() {
        for (MyActionListener listener : listeners) {
            listener.actionPerformed();
        }
    }

    // например, обработчик нажатия на кнопку в этой панели
    public void event() {
        notifyListeners();
    }
}