package playground;

import org.sqlite.SQLiteDataSource;

import java.sql.*;
import java.util.Scanner;


public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {
        try { //sql
            String jdbcUrl = "jdbc:sqlite:/home/smolentsev/Documents/java/DataBases/Playground/Playground.sqlite";
            SQLiteDataSource ds = new SQLiteDataSource();
            ds.setUrl(jdbcUrl);

            try (Connection conn = ds.getConnection()) {
                conn.setAutoCommit(false);
                UserData userData = login(conn);

                int is_admin = userData.isAdmin();
                if (is_admin == 0) {
                    System.out.println("Logged in as user " + userData.getId());
                } else if (is_admin == 1) {
                    printUsersList(conn);
                    chooseAdminAction(conn);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static UserData login(Connection conn) throws SQLException {
        String login;
        int is_admin;
        int id;

        authenticationLoop:
        while (true) { //пока не залогинится корректным образом
            System.out.print("login: ");
            login = scanner.nextLine();
            System.out.print("password: ");
            String password = scanner.nextLine();

            String sql = "SELECT id, is_admin FROM Users WHERE login=? AND password=?";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, login);
                stmt.setString(2, password);
                try (ResultSet rs = stmt.executeQuery()) {
                    if (!rs.next()) {
                        System.out.println("Login failed, try again.");
                        continue authenticationLoop;
                    } else {
                        id = rs.getInt("id");
                        is_admin = rs.getInt("is_admin");
                        break authenticationLoop;
                    }
                }
            }
        }
        return new UserData(id, login, is_admin);
    }

    private static void printUsersList(Connection conn) throws SQLException {
        try (Statement stmt2 = conn.createStatement()) {
            try (ResultSet rs2 = stmt2.executeQuery("SELECT * FROM Users")) {
                System.out.println("List of users:");
                while (rs2.next()) {
                    int id2 = rs2.getInt("id");
                    String login2 = rs2.getString("login");
                    System.out.println("id=" + id2 + ", login=" + login2);
                }
            }
        }
    }

    private static void chooseAdminAction(Connection conn) throws SQLException {
        while (true) { //action loop
            System.out.print("choose admin action: \n" +
                    "1 - add user \n" +
                    "2 - delete user \n" +
                    "3 - edit user \n" +
                    "4 - print users list \n" +
                    "5 - exit \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) { //в левой части может быть возвращаемое значение!
                case 1 -> addUser(conn);
                case 2 -> deleteUser(conn);
                case 3 -> editUser(conn);
                case 4 -> printUsersList(conn);
                case 5 -> {
                    return;
                }
                default -> throw new IllegalStateException("Unexpected value: " + response);
            }
        }
    }

    private static void addUser(Connection conn) throws SQLException {
        System.out.println("input new login or enter to escape");
        String login = scanner.nextLine(); //почему проскакивает?
        if (login.isEmpty()) return; //если нажат пустой энтер, то возврат в chooseAdminAction
        System.out.print("input new password:");
        String password = scanner.nextLine();
        int is_admin;
        while (true) {
            System.out.println("is admin? \n" +
                    "1 - yes \n" +
                    "2 - no");
            is_admin = scanner.nextInt();
            if (is_admin != 0 && is_admin != 1) {
                System.out.println("incorrect value, expected 0 or 1, try again.");
            } else break;
        }
        //String sql = "INSERT INTO User(login, password, is_admin) VALUES (login=? , password=?, is_admin=?)";
        String sql = "INSERT INTO Users VALUES (login=? , password=?, is_admin=?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, login);
            stmt.setString(2, password);
            stmt.setInt(3, is_admin);
            stmt.execute();
            conn.commit();
        }
    }


    private static void deleteUser(Connection conn) throws SQLException {
        System.out.println("input id of removed user:");
        int id = scanner.nextInt();
        String sql = "DELETE FROM Users WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.execute();
            conn.commit();
        }
    }

    private static void editUser(Connection conn) {
        //сперва найти изменяемого юзера

        while (true) { //change_loop
            System.out.print("choose edit action: \n" +
                    "1 - edit login \n" +
                    "2 - edit password \n" +
                    "3 - edit is_admin \n" +
                    "4 - edit id \n" +
                    "5 - exit");
            int response = scanner.nextInt();
        }

        //написать свитч по респонсу
    }
}

