package playground;

import java.sql.Connection;

public class UserData {
    private int id;
    private String login;
    private int isAdmin;

    public UserData(int id, String login, int isAdmin) {
        this.id = id;
        this.login = login;
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int isAdmin() {
        return isAdmin;
    }

    public void setAdmin(int admin) {
        isAdmin = admin;
    }
}
