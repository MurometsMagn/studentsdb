package playground;

import org.sqlite.SQLiteDataSource;

import java.sql.*;

public class extra {
    public static void exampleSQL() throws SQLException {
        String jdbcUrl = "jdbc:sqlite:/home/smolentsev/Documents/java/DataBases/Playground/Playground.sqlite";
        SQLiteDataSource ds = new SQLiteDataSource(); //с датаСорсем
        ds.setUrl(jdbcUrl);
        //Connection conn = DriverManager.getConnection(jdbcUrl);  //DriverManager

        try (Connection conn = ds.getConnection()) {
            conn.setAutoCommit(false);

            //Для выполнения запросов, не возвращающих данные:
            try (Statement stmt = conn.createStatement()) {
                stmt.execute("CREATE TABLE Test(id INTEGER PRIMARY KEY)");
                conn.commit();
            }

            //Для выполнения запросов, возвращающих данные:
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery("SELECT * FROM Users")) {
                    // используем rs
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        String login = rs.getString("login");
                        /*// или то же самое с номером столбца
                        int id = rs.getInt(0);
                        String login = rs.getString(1);*/
                    }
                    conn.commit();
                }
            }
            String login = "";
            String password = "";
            String sql = "SELECT id FROM Users WHERE login=? AND password=?";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, login);
                stmt.setString(2, password);
                try (ResultSet rs = stmt.executeQuery()) {
                }
            }
        }
    }
}
