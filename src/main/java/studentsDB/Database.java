package studentsDB;

import studentsDB.repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private static Database instance = null;

    private final Connection connection;
    private final String jdbcUrl = "jdbc:sqlite:/home/smolentsev/Documents/java/DataBases/StudyDB/StudyDB.sqlite";
    private final GroupRepository groupRepository;
    private final MarkRepository markRepository;
    private final DepartmentRepository departmentRepository;
    private final FacultyRepository facultyRepository;
    private final SpecialityRepository specialityRepository;
    private final StudentRepository studentRepository;
    private final SubjectRepository subjectRepository;
    private final TeacherRepository teacherRepository;
    private final GetterForNamesById getterForNamesById;
    private final DeleterById deleterById;


    public static Database getInstance() throws SQLException {
        if (instance == null) {
            instance = new Database();
        }

        return instance;
    }

    private Database() throws SQLException {
        //connection = DriverManager.getConnection("jdbc:sqlite::memory:");
        connection = DriverManager.getConnection(jdbcUrl);
        connection.setAutoCommit(false);
        groupRepository = new GroupRepository(connection);
        departmentRepository = new DepartmentRepository(connection);
        teacherRepository = new TeacherRepository(connection);
        subjectRepository = new SubjectRepository(connection);
        studentRepository = new StudentRepository(connection);
        specialityRepository = new SpecialityRepository(connection);
        facultyRepository = new FacultyRepository(connection);
        markRepository = new MarkRepository(connection);
        getterForNamesById = new GetterForNamesById(connection);
        deleterById = new DeleterById(connection);
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public DepartmentRepository getDepartmentRepository() {
        return departmentRepository;
    }

    public TeacherRepository getTeacherRepository() {
        return teacherRepository;
    }

    public SubjectRepository getSubjectRepository() {
        return subjectRepository;
    }

    public StudentRepository getStudentRepository() {
        return studentRepository;
    }

    public SpecialityRepository getSpecialityRepository() {
        return specialityRepository;
    }

    public FacultyRepository getFacultyRepository() {
        return facultyRepository;
    }

    public MarkRepository getMarkRepository() {
        return markRepository;
    }

    public GetterForNamesById getterForNamesById() {
        return getterForNamesById;
    }

    public DeleterById getDeleterById() {
        return deleterById;
    }
}
