package studentsDB.GUI;

import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class LeftPanel extends JPanel { //mediator
    LeftPanelTray leftPanelTray = new LeftPanelTray();
    LeftPanelWorkSpace leftPanelWorkSpace = new LeftPanelWorkSpace();

    public LeftPanel() {
        setLayout(new MigLayout("insets 0, wrap 2",
                "[min!]0[]",
                "[min!]0[]"));
        //add(leftPanelTray, "spany, pushy, growy");
        add(leftPanelTray, new CC().dockWest());
        //add(leftPanelHeader, "pushx, growx");
        add(leftPanelWorkSpace, "push, grow");

        leftPanelTray.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (leftPanelWorkSpace.getWidth() == 0) {
                    leftPanelWorkSpace.setSize(getPreferredSize().width, getHeight());
                } else {
                    leftPanelWorkSpace.setSize(0, 0);
                }
                //lpw.setSize(0 , 0);
                //lpw.add(new JLabel("yflgbcnm"));
                //new LeftPanel().repaint();
                leftPanelWorkSpace.repaint();
            }
        });

//        add(new VerticalButton("Show Content", false), "spany, pushy, growy");
//        add(leftPanelWorkSpace, "push, grow");
    }
}

class LeftPanelTray extends JPanel {
    JButton showContentBtn = new VerticalButton("Show Content", false );
    private java.util.List<ActionListener> listeners = new ArrayList<>();

    public LeftPanelTray() {
        setLayout(new MigLayout("wrap 1",
                "0[min!]0",
                "0[]0"));
        //showContentBtn.setPreferredSize(new Dimension(5, 100));
        add(showContentBtn, "");

//        showContentBtn.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                LeftPanelWorkSpace lpw = new LeftPanelWorkSpace();
//                if (LeftPanelWorkSpace.WIDTH == 0)  lpw.setSize(getPreferredSize().width, getHeight());
//                else lpw.setSize(0, 0);
//                //lpw.setSize(0 , 0);
//                lpw.add(new JLabel("yflgbcnm"));
//                //new LeftPanel().repaint();
//                lpw.repaint();
//                System.out.println("button pressed");
//            }
//        });
        showContentBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (ActionListener actionListener : listeners) {
                    actionListener.actionPerformed(e);
                }
            }
        });

    }

    public void addActionListener(ActionListener actionListener) {
        listeners.add(actionListener);
    }
}

class LeftPanelHeader extends JPanel {
    JButton contentBtn = new JButton("Content");

    public LeftPanelHeader() {
        setLayout(new MigLayout("",
        "0[][][]0",
        "0[10]0"));
        add(contentBtn, "align left");

    }

}

class LeftPanelWorkSpace extends JPanel {
    LeftPanelHeader leftPanelHeader = new LeftPanelHeader();

    public LeftPanelWorkSpace() {
        setLayout(new MigLayout("debug",
                "0[0::]0",
                "0[]0"));
        add(leftPanelHeader);
    }

}
