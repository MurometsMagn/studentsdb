package studentsDB.GUI;

import net.miginfocom.layout.AC;
import net.miginfocom.layout.CC;
import net.miginfocom.layout.LC;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

public class StartFrame extends JFrame {
    MenuPanel menuPanel = new MenuPanel();
    InstrumentalPanel instrumentalPanel = new InstrumentalPanel();
    LeftPanel leftPanel = new LeftPanel();
    WorkSpacePanel workSpacePanel = new WorkSpacePanel();
    RightPanel rightPanel = new RightPanel();
    DownPanel downPanel = new DownPanel();
    StatusBarPanel statusBarPanel = new StatusBarPanel();

    public StartFrame() throws SQLException {
        //порядок не менять:
        super("StudentsDB");
        setSize(1000, 800);
        setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //can be replaced
        setLocationRelativeTo(null);
        setLayout(new MigLayout("wrap 3, insets 0",
                "0[0::pref]0[]0[]0",
                "[][][][][]0"));
//                (new LC().wrapAfter(3).insets("0").debug(),
//                        new AC().grow(),
//                        new AC().gap("0")));
//        add(menuPanel, new CC().dockNorth());
        add(menuPanel, "dock north");
        add(instrumentalPanel, "span3, pushx, growx");
        add(leftPanel, "pushy, growy"); //нужно ограничить горизонтальный рост
        add(workSpacePanel, "push, grow");
        add(rightPanel, "pushy, growy");
        add(downPanel, "span3, pushx, growx");
        add(statusBarPanel, "dock south");

        setVisible(true);
    }
}
