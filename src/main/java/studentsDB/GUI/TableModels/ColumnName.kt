package studentsDB.GUI.TableModels

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class ColumnName(val value: String = "")
