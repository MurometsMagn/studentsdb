package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Department;

import java.sql.SQLException;
import java.util.List;

public class DepartmentTableModel extends MyAbstractTableModel<Department>{

    public DepartmentTableModel() throws SQLException {
        super(Database.getInstance().getDepartmentRepository().getAllDepartments());
        fillCollumnNames(Department.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Department department = data.get(rowIndex);
        String[] str = {String.valueOf(department.id), department.name,
                String.valueOf(department.faculty_id), String.valueOf(department.chairman_id)};
        return str[columnIndex];
    }
}
