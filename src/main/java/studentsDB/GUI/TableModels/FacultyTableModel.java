package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Faculty;

import java.sql.SQLException;
import java.util.List;

public class FacultyTableModel extends MyAbstractTableModel<Faculty> {

    public FacultyTableModel() throws SQLException {
        super(Database.getInstance().getFacultyRepository().getAllFaculties());
        fillCollumnNames(Faculty.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Faculty faculty = data.get(rowIndex);
        String[] str = {String.valueOf(faculty.id), faculty.name};
        return str[columnIndex];
    }
}
