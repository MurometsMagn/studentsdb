package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Group;

import java.sql.SQLException;
import java.util.List;

public class GroupTableModel extends MyAbstractTableModel<Group> {

    public GroupTableModel() throws SQLException {
        super(Database.getInstance().getGroupRepository().getAllGroups());
        fillCollumnNames(Group.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Group group = data.get(rowIndex);
        String[] str = {group.id, group.name, group.speciality_id,
                String.valueOf(group.cource_nmb), String.valueOf(group.headman_id)};
        return str[columnIndex];
    }
}
