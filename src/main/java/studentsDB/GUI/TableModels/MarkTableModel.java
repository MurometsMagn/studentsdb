package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Department;
import studentsDB.models.Mark;
import studentsDB.models.Student;

import java.sql.SQLException;
import java.util.List;

public class MarkTableModel extends MyAbstractTableModel<Mark> {

    public MarkTableModel(int student_id) throws SQLException{
        super(Database.getInstance().getMarkRepository().getAllMarksForStudent(student_id));
        fillCollumnNames(Mark.class);
    }

    public MarkTableModel(int subject_id, String group_id) throws SQLException{
        super(Database.getInstance().getMarkRepository().getAllMarksForSubject(subject_id, group_id));
        fillCollumnNames(Mark.class);
    }

//    public MarkTableModel() throws SQLException {
//        super(Database.getInstance().getMarkRepository().getAllMarksForStudent());
//        fillCollumnNames(Mark.class);
//    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Mark mark = data.get(rowIndex);
        String[] str = {mark.id + "", mark.value + "",
                mark.student_id.toString(), mark.subject_id + ""};
        return str[columnIndex];
    }
}
