package studentsDB.GUI.TableModels;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class MyAbstractTableModel<T> implements TableModel {
    private Set<TableModelListener> listeners = new HashSet<>();
    protected String[] columnName;
    protected List<T> data;

    public MyAbstractTableModel(List<T> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnName[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    //обязательно переопределять в потомках:
//    @Override
//    public Object getValueAt(int rowIndex, int columnIndex) {
//        return null;
//    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    protected void fillCollumnNames(Class<?> modelClass) {
        Field[] fields = modelClass.getFields();
        columnName = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].isAnnotationPresent(ColumnName.class)) {
                ColumnName columnNameAnnotation = fields[i].getAnnotation(ColumnName.class);
                columnName[i] = columnNameAnnotation.value();
            } else {
                columnName[i] = fields[i].getName(); //reflection
            }
        }
    }
}
/*20.05.21
 1. Создать класс аннотаций ColumnName применяемый к полям класса
 2. Пометить поля класса Стьюдентс этой аннотацией, где поле названо по другому
 3. Дополнить алгоритм получения названий столбцов проверкой наличия у столбца аннотаций ColumnName :
 если аннотация есть - использовать ее как имя столбца, иначе использовать имя поля как имя столбца.
 https://dzone.com/articles/creating-custom-annotations-in-java
 */


/*
 часто используемой стр-рой классов является:
 1. интерфейс - определяет набор методов, которые должны быть в конкретных реализациях (определяет поведение)
 2. абстракный класс - реализует интерфейс и определяет реализацию некоторых методов интерфейса,
 которые часто будут реализованы именно таким образом (устраняют дублирование кода)
 3. конкретный класс - наследует абстрактный класс и реализует оставшиеся методы интерфейса,
 также может переопределить некоторые методы абстрактного класса если их реализация ему не подходит.
 (определяет реализацию).

 Различие между абстрактным методом и пустым нефинальным методом:
 - абстрактный метод обязывает наследника его переопределить
 - пустой метод дает возможность его переопределить, но не заставляет это делать
 В ситуации, когда требуется определение более одного метода, но некоторые из этих методов могут быть пустыми,
 стоит предпочесть пустые нефинальные методы (например определить их в абстрактном классе)

 Если есть возможность не делать метод абстрактным - то не стоит делать его абстрактным:
 метод абстрактным должен быть только тогда, когда наследники должны предоставить какую-то
 осмысленную его реализацию (пустая или тревиальная не подойдет( тривиальная - return 0 etc)).
 */