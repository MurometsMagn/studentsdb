package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Group;
import studentsDB.models.Speciality;

import java.sql.SQLException;
import java.util.List;

public class SpecialityTableModel extends MyAbstractTableModel<Speciality>{

    public SpecialityTableModel() throws SQLException {
        super(Database.getInstance().getSpecialityRepository().getAllSpecialities());
        fillCollumnNames(Speciality.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Speciality speciality = data.get(rowIndex);
        String[] str = {speciality.id, speciality.name, speciality.department_id + ""};
        return str[columnIndex];
    }
}
