package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Student;
import studentsDB.repositories.StudentRepository;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StudentTableModel extends MyAbstractTableModel<Student> {

    private final String[] columnName = {"id", "firstName", "middleName", "lastName", "group_id"};
    //private StudentRepository studentRepository = Database.getInstance().getStudentRepository();
    private final List<Student> allStudents = Database.getInstance().getStudentRepository().getAllStudents();
    //убрать allStudents, вместо него использовать data

    public StudentTableModel() throws SQLException {
        super(Database.getInstance().getStudentRepository().getAllStudents());
        fillCollumnNames(Student.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
//        Student student = allStudents.get(rowIndex);
//        String[] str = {String.valueOf(student.id), student.firstName, student.middleName,
//                student.lastName, student.group_id};
//        return str[columnIndex];

        Student student = data.get(rowIndex);
        String[] str = {String.valueOf(student.id), student.firstName, student.middleName,
                student.lastName, student.group_id};
        return str[columnIndex];
    }
}
