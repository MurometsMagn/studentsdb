package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Mark;
import studentsDB.models.Subject;

import java.sql.SQLException;
import java.util.List;

public class SubjectTableModel extends MyAbstractTableModel<Subject> {

    public SubjectTableModel() throws SQLException {
        super(Database.getInstance().getSubjectRepository().getAllSubjects());
        fillCollumnNames(Subject.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Subject subject = data.get(rowIndex);
        String[] str = {subject.id + "", subject.name,
                subject.subject_type_id + "", subject.needs_mark + ""};
        return str[columnIndex];
    }
}
