package studentsDB.GUI.TableModels;

import studentsDB.Database;
import studentsDB.models.Teacher;

import java.sql.SQLException;
import java.util.List;

public class TeacherTableModel extends MyAbstractTableModel<Teacher> {

    public TeacherTableModel() throws SQLException {
        super(Database.getInstance().getTeacherRepository().getAllTeachers());
        fillCollumnNames(Teacher.class);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Teacher teacher = data.get(rowIndex);
        String[] str = {teacher.id + "", teacher.firstName, teacher.middleName, teacher.lastName,
                teacher.department_id + "", teacher.position, teacher.scientificDegree};
        return str[columnIndex];
    }
}
