package studentsDB.GUI;

import net.miginfocom.swing.MigLayout;
import studentsDB.GUI.TableModels.StudentTableModel;

import javax.swing.*;
import java.sql.SQLException;

public class WorkSpacePanel extends JPanel {
    JTable studentTable = new JTable(new StudentTableModel());
    JScrollPane jScrollPane = new JScrollPane(studentTable);

    public WorkSpacePanel() throws SQLException {
        setLayout(new MigLayout());

        //add(studentTable);
        add(jScrollPane);
    }
}
