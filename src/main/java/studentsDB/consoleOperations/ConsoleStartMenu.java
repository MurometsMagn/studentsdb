package studentsDB.consoleOperations;

import java.sql.SQLException;
import java.util.Scanner;

public class ConsoleStartMenu {
    Scanner scanner = new Scanner(System.in);
    DepartmentConsole departmentConsole = new DepartmentConsole();
    FacultyConsole facultyConsole = new FacultyConsole();
    GroupConsole groupConsole = new GroupConsole();
    MarkConsole markConsole = new MarkConsole();
    SpecialityConsole specialityConsole = new SpecialityConsole();
    StudentConsole studentConsole = new StudentConsole();
    SubjectConsole subjectConsole = new SubjectConsole();
    TeacherConsole teacherConsole = new TeacherConsole();

    public ConsoleStartMenu() throws SQLException {
    }

    public void tableMenu() throws SQLException {
        while (true) {
            System.out.println("STUDENTS DataBase. \n" +
                    "Select an item to deal with: \n" +
                    "1 - Department \n" +
                    "2 - Faculty \n" +
                    "3 - Group \n" +
                    "4 - Mark \n" +
                    "5 - Speciality \n" +
                    "6 - Student \n" +
                    "7 - Subject \n" +
                    "8 - Teacher \n" +
                    "9 - exit without saving...");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> departmentConsole.menu();
                case 2 -> facultyConsole.menu();
                case 3 -> groupConsole.menu();
                case 4 -> markConsole.menu();
                case 5 -> specialityConsole.menu();
                case 6 -> studentConsole.menu();
                case 7 -> subjectConsole.menu();
                case 8 -> teacherConsole.menu();
                case 9 -> System.exit(0);
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    public int soutCrudMenu(String tableName) {
        return soutCrudMenu(tableName, tableName + "s");
    }

    //if plural form is unusual, e.g. "specialities"
    public int soutCrudMenu(String tableName, String pluralTableName){
        System.out.println("What would you like to do with the table "
                + tableName + ": \n" +
                "1 - print all " + pluralTableName + "\n" +
                "2 - add new " + tableName + "\n" +
                "3 - delete " + tableName +"\n" +
                "4 - edit " + tableName + "\n" +
                "5 - back to previos menu");
        int response = scanner.nextInt();
        scanner.nextLine(); //чтобы убрать перевод строки
        return response;
    }
}
