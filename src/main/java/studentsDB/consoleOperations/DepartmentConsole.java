package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Department;
import studentsDB.repositories.DepartmentRepository;
import studentsDB.repositories.GetterForNamesById;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class DepartmentConsole {
    private final DepartmentRepository departmentRepository = Database.getInstance().getDepartmentRepository();
    Scanner scanner = new Scanner(System.in);

    public DepartmentConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Department");
            switch (response) {
                case 1 -> printDepartments();
                case 2 -> addDepartment();
                case 3 -> removeDepartment();
                case 4 -> changeMenu();
                case 5 -> {
                    break loop;
                }
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void printDepartments() throws SQLException {
        GetterForNamesById getterForNamesById = Database.getInstance().getterForNamesById();
        //Database db = Database.getInstance();
        List<Department> allDepartments = departmentRepository.getAllDepartments();
        //@MyOunAnnotation //процессор аннотаций
        //Database.getInstance().get + "Faculty" + Repository().getAllDepartments();
        for (Department item : allDepartments) {
            String facultyName = getterForNamesById.getNameById("Faculty", item.faculty_id);
            String chairmanName = getterForNamesById.getFullNameById("Teacher", item.chairman_id);
            System.out.println(item.id + "  " + item.name + ", Faculty: " + facultyName +
                    ", Chairman: " + chairmanName);
        }
        System.out.println();
    }

    private void addDepartment() throws SQLException {
        System.out.println("input new department_name to insert:");
        String name = scanner.nextLine();
        departmentRepository.setDepartment(name);
    }

    private void removeDepartment() throws SQLException {
        System.out.println("input id of removing department:");
        int id = scanner.nextInt();
        departmentRepository.deleteDepartment(id);
    }

    private void changeMenu() throws SQLException {
        while (true) {
            System.out.println("choose action: \n" +
                    "1 - change department_name \n" +
                    "2 - change faculty \n" +
                    "3 - change chairman \n" +
                    "4 - back \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> editDepartment();
                case 2 -> setFaculty();
                case 3 -> setChairman();
                case 4 -> {return;}
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void editDepartment() throws SQLException {
        System.out.println("input id of edited department:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new department_name to update:");
        String name = scanner.nextLine();
        departmentRepository.editDepartment(id, name);
    }

    private void setFaculty() throws SQLException {
        System.out.println("input id of edited Department:");
        int departmentId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new faculty_id:");
        int facultyId = scanner.nextInt();
        departmentRepository.setFacultyId(departmentId, facultyId);
    }

    private void setChairman() throws SQLException {
        System.out.println("input id of edited Department:");
        int departmentId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new chairman_id:");
        int chairmanId = scanner.nextInt();
        departmentRepository.setChairmanId(departmentId, chairmanId);
    }
}
