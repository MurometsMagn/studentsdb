package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Faculty;
import studentsDB.repositories.FacultyRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class FacultyConsole {
    private final FacultyRepository facultyRepository = Database.getInstance().getFacultyRepository();
    Scanner scanner = new Scanner(System.in);

    public FacultyConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Faculty", "Faculties");
            switch (response) {
                case 1 -> printFaculties();
                case 2 -> addFaculty();
                case 3 -> removeFaculty();
                case 4 -> editFaculty();
                case 5 -> {break loop;}
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void printFaculties() throws SQLException {
        List<Faculty> allFaculties = facultyRepository.getAllFaculties();
        for (Faculty item : allFaculties) {
            System.out.println(item.id + "  " + item.name.toString());
        }
        System.out.println();
    }

    private void addFaculty() throws SQLException {
        System.out.println("input new Faculty_name to insert:");
        String name = scanner.nextLine();
        facultyRepository.setFaculty(name);
    }

    private void removeFaculty() throws SQLException {
        System.out.println("input id of removing Faculty:");
        int id = scanner.nextInt();
        facultyRepository.deleteFaculty(id);
    }

    private void editFaculty() throws SQLException {
        System.out.println("input id of edited Faculty:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new Faculty_name to update:");
        String name = scanner.nextLine();
        facultyRepository.editFaculty(id, name);
    }
}
