package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Group;
import studentsDB.repositories.GroupRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class GroupConsole {
    private final GroupRepository groupRepository = Database.getInstance().getGroupRepository();
    Scanner scanner = new Scanner(System.in);

    public GroupConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Group");
            switch (response) {
                case 1 -> printGroups();
                case 2 -> addGroup();
                case 3 -> removeGroup();
                case 4 -> changeMenu();
                case 5 -> {
                    break loop;
                }
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void printGroups() throws SQLException {
        List<Group> allGroups = groupRepository.getAllGroups();
        for (Group item : allGroups) {
            String speciality = Database.getInstance().getterForNamesById()
                    .getNameById("Speciality", item.speciality_id);
            String headman = Database.getInstance().getterForNamesById()
                    .getFullNameById("Student", item.headman_id);
            System.out.println(item.id + "  " + item.name + ", speciality: " + speciality + ", courceNmb: " +
                    item.cource_nmb + ", headman: " + headman);
        }
        System.out.println();
    }

    private void addGroup() throws SQLException {
        System.out.println("input new group_id to insert:");
        String id = scanner.nextLine();
        //scanner.nextLine();
        System.out.println("input new group_name to insert:");
        String name = scanner.nextLine();
        groupRepository.setGroup(id, name);
    }

    private void removeGroup() throws SQLException {
        System.out.println("input id of removing group:");
        String id = scanner.nextLine();
        groupRepository.deleteGroup(id);
    }

    private void changeMenu() throws SQLException {
        while (true) {
            System.out.println("choose action: \n" +
                    "1 - change group_name \n" +
                    "2 - change speciality \n" +
                    "3 - set cource number \n" +
                    "4 - set headman \n" +
                    "5 - back \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> editGroup();
                case 2 -> setSpeciality();
                case 3 -> setCourceNmb();
                case 4 -> setHeadman();
                case 5 -> {
                    return;
                }
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void setSpeciality() throws SQLException {
        System.out.println("input id of edited Group:");
        String groupId = scanner.nextLine();
        //scanner.nextLine();
        System.out.println("input new speciality_id:");
        String specialityId = scanner.nextLine();
        groupRepository.setSpecialityId(groupId, specialityId);
    }

    private void editGroup() throws SQLException {
        System.out.println("input id of edited group:");
        String id = scanner.nextLine();
        System.out.println("input new group_name to update:");
        String name = scanner.nextLine();
        groupRepository.editGroup(id, name);
    }

    private void setCourceNmb() throws SQLException {
        System.out.println("input id of edited group:");
        String id = scanner.nextLine();
        System.out.println("input new cource_number to update:");
        Integer courceNmb = scanner.nextInt();
        scanner.nextLine();
        groupRepository.setCourceNmb(id, courceNmb);
    }

    private void setHeadman() throws SQLException {
        System.out.println("input id of edited group:");
        String id = scanner.nextLine();
        System.out.println("input new headman_id to update:");
        Integer headman_id = scanner.nextInt();
        scanner.nextLine();
        groupRepository.setHeadman(id, headman_id);
    }
}
