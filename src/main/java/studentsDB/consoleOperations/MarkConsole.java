package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Mark;
import studentsDB.repositories.MarkRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class MarkConsole {
    private final MarkRepository markRepository = Database.getInstance().getMarkRepository();
    Scanner scanner = new Scanner(System.in);

    public MarkConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Mark");
            switch (response) {
                case 1 -> printMarksMenu();
                case 2 -> addMarkMenu();
                case 3 -> removeMark();
                case 4 -> changeMark();
                case 5 -> {
                    break loop;
                }
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void removeMark() throws SQLException {
        System.out.println("input mark_id to delete:");
        int mark_id = scanner.nextInt();
        markRepository.deleteMark(mark_id);
    }

    private void changeMark() throws SQLException {
        System.out.println("input mark_id to edit:");
        int mark_id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new value of the mark:");
        int newValue = scanner.nextInt();
        markRepository.changeMark(mark_id, newValue);
    }

    private void printMarksMenu() throws SQLException {
        while (true) {
            System.out.println("Choose which marks to print: \n" +
                    "1 - all marks for student \n" +
                    "2 - all marks for subject \n" +
                    "3 - back to previos menu");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> {
                    System.out.println("input student_id, whose marks to print:");
                    int student_id = scanner.nextInt();
                    List<Mark> marks = markRepository.getAllMarksForStudent(student_id);
                    System.out.println("Marks of student with student_id: " + student_id + ":");
                    for (Mark item : marks) {
                        System.out.println(item.id + "  " + item.value);
                    }
                }
                case 2 -> {
                    System.out.println("input subject_id, which marks to print:");
                    int subject_id = scanner.nextInt();
                    System.out.println("input group_id, which marks to print:");
                    String group_id = scanner.nextLine();
                    List<Mark> marks = markRepository.getAllMarksForSubject(subject_id, group_id);
                    System.out.println("Marks for subject with subject_id: " + subject_id +
                            "of group with group_id: " + group_id + ":");
                    for (Mark item : marks) {
                        System.out.println(item.id + "  " + item.value);
                    }
                }
                case 3 -> {
                    return;
                }
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void addMarkMenu() throws SQLException {
        while (true) {
            System.out.println("Choose what kind of mark you'd like to add: \n" +
                    "1 - mark \n" +
                    "2 - pass \n" +
                    "3 - back to previos menu");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> {
                    System.out.println("input student_id, whose marks to add:");
                    int student_id = scanner.nextInt();
                    System.out.println("input subject_id, which marks to add:");
                    int subject_id = scanner.nextInt();
                    System.out.println("input value of the mark:");
                    int value = scanner.nextInt();
                    markRepository.addMark(student_id, subject_id, value);
                }
                case 2 -> {
                    System.out.println("input student_id, whose pass to add:");
                    int student_id = scanner.nextInt();
                    System.out.println("input subject_id, which pass to add:");
                    int subject_id = scanner.nextInt();
                    markRepository.addPass(student_id, subject_id);
                }
                case 3 -> {
                    return;
                }
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }
}

/*
прайвит поля (?)
обработка пустого нажатия (DepartmentConsole) (?)
нужно ли из кейсов код убрать  в методы?
нужно ли из внешних ключей брать названия?
 */
