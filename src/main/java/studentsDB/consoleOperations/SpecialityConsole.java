package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Speciality;
import studentsDB.repositories.SpecialityRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class SpecialityConsole {
    private final SpecialityRepository specialityRepository = Database.getInstance().getSpecialityRepository();
    Scanner scanner = new Scanner(System.in);

    public SpecialityConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Speciality", "Specialities");
            switch (response) {
                case 1 -> printSpeciality();
                case 2 -> addSpeciality();
                case 3 -> removeSpeciality();
                case 4 -> changeMenu();
                case 5 -> {
                    break loop;
                }
                default -> {//throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void changeMenu() throws SQLException {
        while (true) {
            System.out.println("choose action: \n" +
                    "1 - change speciality_name \n" +
                    "2 - change department \n" +
                    "3 -  back \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> editSpeciality();
                case 2 -> setDepartment();
                case 3 ->  {
                    return;
                }
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void setDepartment() throws SQLException {
        System.out.println("input id of edited speciality:");
        String specialityId = scanner.nextLine();
        System.out.println("input new department_id:");
        int departmentId = scanner.nextInt();
        scanner.nextLine();
        specialityRepository.setDepartmentId(specialityId, departmentId);
    }

    private void printSpeciality() throws SQLException {
        List<Speciality> allSpecialities = specialityRepository.getAllSpecialities();
        for (Speciality item : allSpecialities) {
            String depName = Database.getInstance().getterForNamesById()
                        .getNameById("Department", (Integer) item.department_id);
            System.out.println(item.id + "  " + item.name + ", department: " + depName);
        }
        System.out.println();
    }

    private void addSpeciality() throws SQLException {
        System.out.println("input new Speciality_id to insert:");
        String id = scanner.nextLine();
        System.out.println("input new Speciality_name to insert:");
        String name = scanner.nextLine();
        specialityRepository.setSpeciality(id, name);
    }

    private void removeSpeciality() throws SQLException {
        System.out.println("input id of removing Speciality:");
        String id = scanner.nextLine();
        specialityRepository.deleteSpeciality(id);
    }

    private void editSpeciality() throws SQLException {
        System.out.println("input id of edited Speciality:");
        String id = scanner.nextLine();
        System.out.println("input new Speciality_name to update:");
        String name = scanner.nextLine();
        specialityRepository.editSpeciality(id, name);
    }
}
