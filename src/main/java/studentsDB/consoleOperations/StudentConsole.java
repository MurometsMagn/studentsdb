package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Student;
import studentsDB.repositories.StudentRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class StudentConsole {
    private final StudentRepository studentRepository = Database.getInstance().getStudentRepository();
    Scanner scanner = new Scanner(System.in);

    public StudentConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Student");
            switch (response) {
                case 1 -> printStudents();
                case 2 -> addStudent();
                case 3 -> removeStudent();
                case 4 -> changeMenu();
                case 5 -> {break loop;}
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void printStudents() throws SQLException {
        List<Student> allStudents = studentRepository.getAllStudents();
        printHeader();
        for (Student item : allStudents) {
            printStudent(item);
            //printStudentUnformated(item);
        }
    }

    private void removeStudent() throws SQLException {
        System.out.println("input id of removing Student:");
        int id = scanner.nextInt();
        studentRepository.deleteStudent(id);
    }

    private void addStudent() throws SQLException {
        System.out.println("input new Student_firstName to insert:");
        String firstName = scanner.nextLine();
        System.out.println("input new Student_middleName to insert:");
        String middleName = scanner.nextLine();
        System.out.println("input new Student_lastName to insert:");
        String lastName = scanner.nextLine();
        studentRepository.setStudent(firstName, middleName, lastName);
    }

    private void changeMenu() throws SQLException {
        while (true) {
            System.out.println("choose action: \n" +
                    "1 - change first name \n" +
                    "2 - change middle name \n" +
                    "3 - change last name \n" +
                    "4 - set group_id \n" +
                    "5 - exit \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> changeFirstName();
                case 2 -> changeMiddleName();
                case 3 -> changeLastName();
                case 4 -> setGroupId();
                case 5 -> {
                    return;
                }
                default -> throw new IllegalStateException("Unexpected value: " + response);
            }
        }
    }

    private void changeFirstName() throws SQLException {
        System.out.println("input id of edited Student:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new first name:");
        String firstName = scanner.nextLine();
        studentRepository.changeFirstName(id, firstName);
    }

    private void changeMiddleName() throws SQLException {
        System.out.println("input id of edited Student:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new middle name:");
        String middleName = scanner.nextLine();
        studentRepository.changeMiddleName(id, middleName);
    }

    private void changeLastName() throws SQLException {
        System.out.println("input id of edited Student:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new last name:");
        String lastName = scanner.nextLine();
        studentRepository.changeLastName(id, lastName);
    }

    private void setGroupId() throws SQLException {
        System.out.println("input id of edited Student:");
        int student_id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new group_id:");
        String group_id = scanner.nextLine();
        studentRepository.setGroupId(student_id, group_id);
    }

    private void printStudentUnformated(Student student) {
        StringBuilder sb = new StringBuilder();
        sb.append(student.id);
        sb.append(" ");
        sb.append(student.lastName);
        sb.append(" ");
        sb.append(student.firstName);
        sb.append(" ");
        sb.append(student.middleName);
        System.out.println(sb.toString());
    }

    private void printStudent(Student student) {
        String format = formatting();
        String studentN = String.format(format, String.valueOf(student.id), student.lastName, student.firstName,
                student.middleName, student.group_id);
        System.out.println(studentN);
    }

    private String formatting() {
        //int tmpLng = Repozitory.strLength; //25
        int tmpLng = 25;
        String format = "%1$-" + 15 + "s %2$-" + tmpLng + "s %3$-" +
                tmpLng + "s %4$-" + tmpLng + "s %5$-" + tmpLng + "s";
        //String format = "%1$-15s %2$-15s %3$-15s %4$-15s %5$-15s %6$-15s";
        return format;
    }

    private void printHeader() {
        String format = formatting();
        String header = String.format(format, "ID", "LastName", "FirstName", "MiddleName" , "Group_id");
        System.out.println(header);
    }
}

