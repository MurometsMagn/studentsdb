package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Subject;
import studentsDB.repositories.SubjectRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class SubjectConsole {
    private final SubjectRepository subjectRepository = Database.getInstance().getSubjectRepository();
    Scanner scanner = new Scanner(System.in);

    public SubjectConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Subject");
            switch (response) {
                case 1 -> printSubjects();
                case 2 -> addSubject();
                case 3 -> removeSubject();
                case 4 -> changeMenu();
                case 5 -> {break loop;}
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void changeMenu() throws SQLException {
        while (true) {
            System.out.println("choose action: \n" +
                    "1 - change subject name \n" +
                    "2 - setSubjectType \n" +
                    "3 - back \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> editSubject();
                case 2 -> setSubjectType();
                case 3 -> {return;}
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void setSubjectType() throws SQLException {
        int subject_type;
        System.out.println("input id of edited subject:");
        int subject_id = scanner.nextInt();
        scanner.nextLine();
        while (true) {
            System.out.println("input new subject_type (0 or 1):");
            subject_type = scanner.nextInt();
            if (subject_type == 0 || subject_type == 1) break;
            System.out.println("Unexpected value: " + subject_type + ", try again:");
        }
        subjectRepository.setSubjectType(subject_id, subject_type);
    }

    private void printSubjects() throws SQLException {
        List<Subject> allSubjects = subjectRepository.getAllSubjects();
        for (Subject item : allSubjects) {
            System.out.println(item.id + "  " + item.name + ", subject_type need_marks = " + item.needs_mark);
        }
        System.out.println("");
    }

    private void addSubject() throws SQLException {
        //scanner.nextLine(); //чтобы убрать перевод строки
        System.out.println("input new Subject_name to insert:");
        String name = scanner.nextLine();
        subjectRepository.setSubject(name);
    }

    private void removeSubject() throws SQLException {
        System.out.println("input id of removing Subject:");
        int id = scanner.nextInt();
        subjectRepository.deleteSubject(id);
    }

    private void editSubject() throws SQLException {
        System.out.println("input id of edited Subject:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new Subject_name to update:");
        String name = scanner.nextLine();
        subjectRepository.editSubject(id, name);
    }
}
