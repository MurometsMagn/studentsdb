package studentsDB.consoleOperations;

import studentsDB.Database;
import studentsDB.models.Teacher;
import studentsDB.repositories.TeacherRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class TeacherConsole {
    Scanner scanner = new Scanner(System.in);
    private final TeacherRepository teacherRepository = Database.getInstance().getTeacherRepository();

    public TeacherConsole() throws SQLException {
    }

    public void menu() throws SQLException {
        loop:
        while (true) {
            int response = new ConsoleStartMenu().soutCrudMenu("Teacher");
            switch (response) {
                case 1 -> printTeachers();
                case 2 -> addTeacher();
                case 3 -> removeTeacher();
                case 4 -> changeMenu();
                case 5 -> {break loop;}
                default -> {
                    //throw new IllegalStateException("Unexpected value: " + response);
                    System.out.println("Unexpected value: " + response + ", try again:");
                }
            }
        }
    }

    private void removeTeacher() throws SQLException {
        System.out.println("input id of removing Teacher:");
        int id = scanner.nextInt();
        teacherRepository.deleteTeacher(id);
    }

    private void printTeachers() throws SQLException {
        List<Teacher> allTeachers = teacherRepository.getAllTeachers();
        printHeader();
        for (Teacher item: allTeachers){
            printTeacher(item);
        }
    }

    private void addTeacher() throws SQLException {
        scanner.nextLine(); //чтобы убрать перевод строки
        System.out.println("input new Teacher_firstName to insert:");
        String firstName = scanner.nextLine();
        System.out.println("input new Teacher_middleName to insert:");
        String middleName = scanner.nextLine();
        System.out.println("input new Teacher_lastName to insert:");
        String lastName = scanner.nextLine();
        teacherRepository.setTeacher(firstName, middleName, lastName);
    }

    private void changeMenu() throws SQLException {
        while (true) {
            System.out.println("choose action: \n" +
                    "1 - change first name \n" +
                    "2 - change middle name \n" +
                    "3 - change last name \n" +
                    "4 - set department \n" +
                    "5 - set position \n" +
                    "6 - set scientific degree \n" +
                    "7 - exit \n");
            int response = scanner.nextInt();
            scanner.nextLine(); //чтобы убрать перевод строки

            switch (response) {
                case 1 -> changeFirstName();
                case 2 -> changeMiddleName();
                case 3 -> changeLastName();
                case 4 -> setDepartment();
                case 5 -> setPosition();
                case 6 -> setScientificDegree();
                case 7 -> {return;}
                default -> System.out.println("Unexpected value: " + response + ", try again:");
            }
        }
    }

    private void changeFirstName() throws SQLException {
        System.out.println("input id of edited Teacher:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new first name:");
        String firstName = scanner.nextLine();
        teacherRepository.changeFirstName(id, firstName);
    }

    private void changeMiddleName() throws SQLException {
        System.out.println("input id of edited Teacher:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new middle name:");
        String middleName = scanner.nextLine();
        teacherRepository.changeMiddleName(id, middleName);
    }

    private void changeLastName() throws SQLException {
        System.out.println("input id of edited Teacher:");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new last name:");
        String lastName = scanner.nextLine();
        teacherRepository.changeLastName(id, lastName);
    }

    private void setDepartment() throws SQLException {
        System.out.println("input id of edited Teacher:");
        int teacherId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new department_id:");
        int departmentId = scanner.nextInt();
        teacherRepository.setDepartmentId(teacherId, departmentId);
    }

    private void setPosition() throws SQLException {
        System.out.println("input id of edited Teacher:");
        int teacherId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new position:");
        String position = scanner.nextLine();
        teacherRepository.setPosition(teacherId, position);
    }

    private void setScientificDegree() throws SQLException {
        System.out.println("input id of edited Teacher:");
        int teacherId = scanner.nextInt();
        scanner.nextLine();
        System.out.println("input new scientific degree:");
        String scientificDegree = scanner.nextLine();
        teacherRepository.setScientificDegree(teacherId, scientificDegree);
    }

    private void printTeacher(Teacher teacher) {
        String format = formatting();
        String id = String.valueOf(teacher.id);
        String teacherN = String.format(format, id, teacher.lastName, teacher.firstName,
                teacher.middleName, teacher.department_id, teacher.position, teacher.scientificDegree);
        System.out.println(teacherN);
    }

    private String formatting() {
        //int tmpLng = Repozitory.strLength; //25
        int tmpLng = 15;
        String format = "%1$-" + 7 + "s %2$-" + tmpLng + "s %3$-" +
                tmpLng + "s %4$-" + tmpLng + "s %5$-" + tmpLng + "s %6$-" +
                tmpLng + "s %7$-" + tmpLng + "s";
        //String format = "%1$-15s %2$-15s %3$-15s %4$-15s %5$-15s %6$-15s";
        return format;
    }

    private void printHeader() {
        String format = formatting();
        String header = String.format(format, "ID", "LastName", "FirstName", "MiddleName", "Department", "Position", "ScientificDegree");
        System.out.println(header);
    }
}
