package studentsDB.models;

import studentsDB.GUI.TableModels.ColumnName;

public class Department {
    public int id;
    public String name;
    @ColumnName("faculty id")
    public Integer faculty_id = null;
    public Integer chairman_id;

    public Department(int id, String name) {
        this(id, name, null, null);
    }

    public Department(int id, String name, Integer faculty_id, Integer chairman_id) {
        this.id = id;
        this.name = name;
        this.faculty_id = faculty_id;
        this.chairman_id = chairman_id;
    }
}
