package studentsDB.models;

public class Faculty {
    public int id;
    public String name;

    public Faculty(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
