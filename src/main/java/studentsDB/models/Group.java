package studentsDB.models;

public class Group {
    public String id;
    public String name;
    public String speciality_id;
    public Integer cource_nmb;
    public Integer headman_id;

    public Group(String id, String name, String speciality_id, Integer cource_nmb, Integer headman_id ) {
        this.id = id;
        this.name = name;
        this.speciality_id = speciality_id;
        this.cource_nmb = cource_nmb;
        this.headman_id = headman_id;
    }

    public Group(String id, String name) {
        this(id, name, null, null, null);
    }
}
