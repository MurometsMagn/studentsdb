package studentsDB.models;

public class Mark {
    public int id;
    public Integer value;
    public Integer student_id;
    public Integer subject_id;

    public Mark(int id, Integer value) {
        this(id, value, null, null);
    }

    public Mark(int id, Integer value, Integer student_id, Integer subject_id) {
        this.id = id;
        this.value = value;
        this.student_id = student_id;
        this.subject_id = subject_id;
    }
}


/*
getDepartmentNameById дублирован в TeacherRepository и в SpecialityRepository
 */
