package studentsDB.models;

public class Speciality {
    public String id;
    public String name;
    public Integer department_id;

    public Speciality(String id, String name) {
        this(id, name, null);
    }

    public Speciality(String id, String name, Integer department_id) {
        this.id = id;
        this.name = name;
        this.department_id = department_id;
    }
}
