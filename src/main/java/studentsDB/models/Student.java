package studentsDB.models;

import java.util.Map;

public class Student {
    public int id;
    public String firstName;
    public String middleName;
    public String lastName;
    public String group_id;

    public Student(int id, String lastName, String firstName, String middleName) {
        this(id, lastName, firstName, middleName, null);
    }

    public Student(int id, String lastName, String firstName, String middleName, String group_id) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.group_id = group_id;
    }
}
