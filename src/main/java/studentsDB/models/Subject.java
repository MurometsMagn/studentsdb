package studentsDB.models;

public class Subject {
    public int id;
    public String name;
    public Integer subject_type_id;
    public Boolean needs_mark;

    public Subject(int id, String name) {
        this(id, name, 0, true);
    }

    public Subject(int id, String name, Integer subject_type_id,
                   boolean needs_mark) {
        this.id = id;
        this.name = name;
        this.subject_type_id = subject_type_id;
        this.needs_mark = needs_mark;
    }
}
