package studentsDB.models;

public class Teacher {
    public int id;
    public String firstName;
    public String middleName = null;
    public String lastName;
    public Integer department_id;
    public String position;
    public String scientificDegree;

    public Teacher(int id, String lastName, String firstName, String middleName) {
        this(id, lastName, firstName, middleName, null, null, null);
    }

    public Teacher(int id, String lastName, String firstName, String middleName,
                   Integer department_id, String position, String scientificDegree) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.department_id = department_id;
        this.position = position;
        this.scientificDegree = scientificDegree;
    }
}
