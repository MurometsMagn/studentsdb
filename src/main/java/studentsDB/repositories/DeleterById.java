package studentsDB.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleterById {
    private Connection conn;

    public DeleterById(Connection conn) throws SQLException {
        this.conn = conn;
    }

    private String getNameSQL(String tableName) {
        return "DELETE FROM " + tableName + " WHERE id=? ";
    }

    //если удаление удачно - то возвращает true
    public boolean deleteById(String tableName, Integer id) throws SQLException {
        if (id == null || id == 0) return false;
        try (PreparedStatement stmt = conn.prepareStatement(getNameSQL(tableName))) {
            stmt.setInt(1, id);
            stmt.execute();
            conn.commit();
        }
        return true;
    }

    public boolean deleteById(String tableName, String id)throws SQLException {
        if (id == null || id.trim().isEmpty()) return false;
        try (PreparedStatement stmt = conn.prepareStatement(getNameSQL(tableName))) {
            stmt.setString(1, id);
            stmt.execute();
            conn.commit();
        }
        return true;
    }
}
