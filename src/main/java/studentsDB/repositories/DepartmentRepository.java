package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Department;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DepartmentRepository {
    private final Connection conn;
    private static final String getAllDepartmentsSQL = "SELECT * FROM Department";
    private static final String setDepartmentSQL = "INSERT INTO Department (name) VALUES (?)";
    private static final String deleteDepartmentSQL = "DELETE FROM Department WHERE id=?";
    private static final String updateDepartmentSQL = "UPDATE Department SET name=? WHERE id=?";
    private static final String updateFacultyIdSQL = "UPDATE Department SET faculty_id=? WHERE id=?";
    private static final String updateChairmanIdSQL = "UPDATE Department SET chairman_id=? WHERE id=?";

    public DepartmentRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Department> getAllDepartments() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllDepartmentsSQL)) {
                List<Department> result = new ArrayList<>();
                while (rs.next()) {
                    Department department = new Department(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getInt("faculty_id"),
                            rs.getInt("chairman_id")
                    );
                    result.add(department);
                }
                return result;
            }
        }
    }

    public void setDepartment(String name) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setDepartmentSQL)) {
            stmt.setString(1, name);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteDepartment(int id) throws SQLException{
        Database.getInstance().getDeleterById().deleteById("Department", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteDepartmentSQL)) {
//            stmt.setInt(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void editDepartment(int id, String name) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(updateDepartmentSQL)) {
            stmt.setString(1, name);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void setFacultyId(int departmentId, int facultyId) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(updateFacultyIdSQL)) {
            stmt.setInt(1, facultyId);
            stmt.setInt(2, departmentId);
            stmt.execute();
            conn.commit();
        }
    }

    public void setChairmanId(int departmentId, int chairmanId) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(updateFacultyIdSQL)) {
            stmt.setInt(1, chairmanId);
            stmt.setInt(2, departmentId);
            stmt.execute();
            conn.commit();
        }
    }
}
