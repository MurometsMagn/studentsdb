package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Faculty;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FacultyRepository {
    private final Connection conn;
    private static final String getAllFacultiesSQL = "SELECT * FROM Faculty";
    private static final String setFacultySQL = "INSERT INTO Faculty (name) VALUES (?)";
    private static final String deleteFacultySQL = "DELETE FROM Faculty WHERE id=?";
    private static final String updateFacultySQL = "UPDATE Faculty SET name=? WHERE id=?";

    public FacultyRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Faculty> getAllFaculties() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllFacultiesSQL)) {
                List<Faculty> result = new ArrayList<>();
                while (rs.next()) {
                    Faculty faculty = new Faculty(
                            rs.getInt("id"),
                            rs.getString("name")
                    );
                    result.add(faculty);
                }
                return result;
            }
        }
    }

    public void setFaculty(String name) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setFacultySQL)) {
            stmt.setString(1, name);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteFaculty(int id) throws SQLException{
        Database.getInstance().getDeleterById().deleteById("Faculty", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteFacultySQL)) {
//            stmt.setInt(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void editFaculty(int id, String name) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(updateFacultySQL)) {
            stmt.setString(1, name);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }
}
