package studentsDB.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetterForNamesById {
    private Connection conn;

    public GetterForNamesById(Connection conn) throws SQLException {
        this.conn = conn;
    }

    private String getNameSQL(String tableName) {
        return "SELECT * FROM " + tableName + " WHERE id=?";
    }

    public String getNameById(String tableName, Integer id) throws SQLException {
        if (id == null || id == 0) return "unsigned";
        String name = null;
        try (PreparedStatement stmt = conn.prepareStatement(getNameSQL(tableName))) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                name = rs.getString("name");
            }
        }
        return name;
    }

    public String getNameById(String tableName, String id) throws SQLException {
        if (id == null || id.trim().isEmpty()) return "unsigned";
        String name = null;
        try (PreparedStatement stmt = conn.prepareStatement(getNameSQL(tableName))) {
            stmt.setString(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                name = rs.getString("name");
            }
        }
        return name;
    }

    public String getFullNameById(String tableName, Integer id) throws SQLException {
        if (id == null || id == 0) return "unsigned";
        String lastName;
        String firstName;
        String middleName = "";
        try (PreparedStatement stmt = conn.prepareStatement(getNameSQL(tableName))) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                lastName = rs.getString("last_name");
                firstName = rs.getString("first_name");
                middleName = rs.getString("middle_name");
            }
        }
        return (lastName + " " + firstName + " " + middleName);
    }
}
