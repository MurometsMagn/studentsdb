package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Group;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GroupRepository {
    private final Connection conn;
    /* from Dmitry:
    INSERT INTO Deparment (name) VALUES (?)
    UPDATE Department SET id=?, name=? WHERE ....
    */
    private static final String getAllGroupsSQL = "SELECT * FROM Groups";
    private static final String setGroupSQL = "INSERT INTO Groups (id, name) VALUES(?,?)";
    private static final String deleteGroupSQL = "DELETE FROM Groups WHERE id=?";
    private static final String updateGroupSQL = "UPDATE Groups SET name=? WHERE id=?";
    private static final String setSpecialityIdSQL = "UPDATE Groups SET speciality_id=? WHERE id=?";
    private static final String setCourceNmbSQL = "UPDATE Groups SET cource_nmb=? WHERE id=?";
    private static final String setHeadmanIdSQL = "UPDATE Groups SET headman_id=? WHERE id=?";

    public GroupRepository(Connection conn) throws SQLException {
        this.conn = conn;
        //conn.setAutoCommit(false);
    }

    public List<Group> getAllGroups() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllGroupsSQL)) {
                List<Group> result = new ArrayList<>();
                while (rs.next()) {
                    Group group = new Group(
                            rs.getString("id"),
                            rs.getString("name"),
                            rs.getString("speciality_id"),
                            rs.getInt("cource_nmb"),
                            rs.getInt("headman_id")
                    );
                    result.add(group);
                }
                return result;
            }
        }
    }

    public void setGroup(String id, String name) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setGroupSQL)) {
            stmt.setString(1, id);
            stmt.setString(2, name);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteGroup(String id) throws SQLException {
        Database.getInstance().getDeleterById().deleteById("Group", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteGroupSQL)) {
//            stmt.setString(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void editGroup(String id, String name) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateGroupSQL)) {
            stmt.setString(1, name);
            stmt.setString(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void setSpecialityId(String groupId, String specialityId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setSpecialityIdSQL)) {
            stmt.setString(1, specialityId);
            stmt.setString(2, groupId);
            stmt.execute();
            conn.commit();
        }
    }

    public void setCourceNmb(String groupId, Integer courceNmb) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setCourceNmbSQL)) {
            stmt.setInt(1, courceNmb);
            stmt.setString(2, groupId);
            stmt.execute();
            conn.commit();
        }
    }

    public void setHeadman(String groupId, Integer headmanId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setHeadmanIdSQL)) {
            stmt.setInt(1, headmanId);
            stmt.setString(2, groupId);
            stmt.execute();
            conn.commit();
        }
    }
}
