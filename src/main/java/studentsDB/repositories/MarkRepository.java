package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Group;
import studentsDB.models.Mark;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MarkRepository {
    private final Connection conn;
    private static final String getAllMarksForStudentSQL = "SELECT * FROM Mark WHERE student_id=?";
    private static final String getAllMarksForSubjectSQL = "SELECT * FROM Mark m INNER JOIN Student s" +
            "ON m.student_id=s.id INNER JOIN Group g ON s.group_id=g.id WHERE m.subject_id=? AND g.id=?";
    private static final String addMarkSQL = "INSERT INTO Mark (student_id, subject_id, value) VALUES (?,?,?)";
    private static final String addPassSQL = "INSERT INTO Mark (student_id, subject_id, value) VALUES (?,?,null)";
    private static final String deleteMarkSQL = "DELETE FROM Mark WHERE id=?";
    private static final String changeMarkSQL = "UPDATE Mark SET value=? WHERE id=?";
    private static final String setStudentSQL = "UPDATE Mark SET student_id=? WHERE id=?";
    private static final String setSubjectSQL = "UPDATE Mark SET subject_id=? WHERE id=?";
    private static final String getStudentNameById = "SELECT * FROM Student WHERE id=?";
    private static final String getSubjectNameById = "SELECT name FROM Subject WHERE id=?";


    public MarkRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Mark> getAllMarksForStudent(int studentId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(getAllMarksForStudentSQL)) {
            stmt.setInt(1, studentId);
            try (ResultSet rs = stmt.executeQuery()) {
                List<Mark> result = new ArrayList<>();
                while (rs.next()) {
                    Mark mark = new Mark(
                            rs.getInt("id"),
                            rs.getInt("value"),
                            rs.getInt("student_id"),
                            rs.getInt("subject_id")
                    );
                    result.add(mark);
                }
                return result;
            }
        }
    }


    public List<Mark> getAllMarksForSubject(int subjectId, String groupId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(getAllMarksForSubjectSQL)) {
            stmt.setInt(1, subjectId);
            stmt.setString(2, groupId);
            try (ResultSet rs = stmt.executeQuery()) {
                List<Mark> result = new ArrayList<>();
                while (rs.next()) {
                    Mark mark = new Mark(
                            rs.getInt("id"),
                            rs.getInt("value"),
                            rs.getInt("student_id"),
                            rs.getInt("subject_id")
                    );
                    result.add(mark);
                }
                return result;
            }
        }
    }

    public void addMark(int studentId, int subjectId, int value) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(addMarkSQL)) {
            stmt.setInt(1, studentId);
            stmt.setInt(2, subjectId);
            stmt.setInt(3, value);
            stmt.execute();
            conn.commit();
        }
    }

    //зачет
    public void addPass(int studentId, int subjectId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(addPassSQL)) {
            stmt.setInt(1, studentId);
            stmt.setInt(2, subjectId);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteMark(int markId) throws SQLException {
        Database.getInstance().getDeleterById().deleteById("Mark", markId);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteMarkSQL)) {
//            stmt.setInt(1, markId);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void changeMark(int markId, int newValue) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(changeMarkSQL)) {
            stmt.setInt(2, markId);
            stmt.setInt(1, newValue);
            stmt.execute();
            conn.commit();
        }
    }

    public void setStudent(int markId, int studentId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setStudentSQL)) {
            stmt.setInt(2, markId);
            stmt.setInt(1, studentId);
            stmt.execute();
            conn.commit();
        }
    }

    public void setSubject(int markId, int subjectId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setSubjectSQL)) {
            stmt.setInt(2, markId);
            stmt.setInt(1, subjectId);
            stmt.execute();
            conn.commit();
        }
    }
}
