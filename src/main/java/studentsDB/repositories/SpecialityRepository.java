package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Speciality;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SpecialityRepository {
    private final Connection conn;
    private static final String getAllSpecialitiesSQL = "SELECT * FROM Speciality";
    private static final String setSpecialitySQL = "INSERT INTO Speciality (id, name) VALUES (?,?)";
    private static final String deleteSpecialitySQL = "DELETE FROM Speciality WHERE id=?";
    private static final String updateSpecialitySQL = "UPDATE Speciality SET name=? WHERE id=?";
    private static final String setDepartmentSQL = "UPDATE Speciality SET department_id=? WHERE id=?";
    //private static final String getDepartmentNameById = "SELECT name FROM Department WHERE id=?";

    public SpecialityRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Speciality> getAllSpecialities() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllSpecialitiesSQL)) {
                List<Speciality> result = new ArrayList<>();
                while (rs.next()) {
                    Speciality speciality = new Speciality(
                            rs.getString("id"),
                            rs.getString("name"),
                             rs.getInt("department_id")
                    );
                    result.add(speciality);
                }
                return result;
            }
        }
    }

//    public String getDepartmentNameById(Integer department_id) throws SQLException {
//        String departmentName = null;
//        try (PreparedStatement stmt = conn.prepareStatement(getDepartmentNameById)) {
//            stmt.setInt(1, department_id);
//            try (ResultSet rs = stmt.executeQuery()) {
//                departmentName = rs.getString("name");
//            }
//        }
//        return departmentName;
//    }

    public void setSpeciality(String id, String name) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setSpecialitySQL)) {
            stmt.setString(1, id);
            stmt.setString(2, name);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteSpeciality(String id) throws SQLException{
        Database.getInstance().getDeleterById().deleteById("Speciality", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteSpecialitySQL)) {
//            stmt.setString(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void editSpeciality(String id, String name) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(updateSpecialitySQL)) {
            stmt.setString(1, name);
            stmt.setString(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void setDepartmentId(String specialityId, int departmentId) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(setDepartmentSQL)) {
            stmt.setInt(1, departmentId);
            stmt.setString(2, specialityId);
            stmt.execute();
            conn.commit();
        }
    }
}
