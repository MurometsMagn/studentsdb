package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentRepository {
    private final Connection conn;
    private static final String getAllStudentsSQL = "SELECT * FROM Student";
    private static final String setStudentSQL = "INSERT INTO Student (first_name, middle_name, last_name) VALUES(?, ?, ?)";
    private static final String deleteStudentSQL = "DELETE FROM Student WHERE id=? ";
    private static final String updateStudentFirstNameSQL = "UPDATE Student SET first_name=? WHERE id=?";
    private static final String updateStudentMiddleNameSQL = "UPDATE Student SET middle_name=? WHERE id=?";
    private static final String updateStudentLastNameSQL = "UPDATE Student SET last_name=? WHERE id=?";
    private static final String setGroupIdSQL = "UPDATE Student SET group_id=? WHERE id=?";

    public StudentRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Student> getAllStudents() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllStudentsSQL)) {
                List<Student> result = new ArrayList<>();
                while (rs.next()) {
                    Student student = new Student(
                            rs.getInt("id"),
                            rs.getString("last_name"),
                            rs.getString("first_name"),
                            rs.getString("middle_name"),
                            rs.getString("group_id")
                    );
                    result.add(student);
                }
                return result;
            }
        }
    }

    public void setStudent(String firstName, String middleName, String lastName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setStudentSQL)) {
            stmt.setString(1, firstName);
            stmt.setString(2, middleName);
            stmt.setString(3, lastName);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteStudent(int id) throws SQLException {
        Database.getInstance().getDeleterById().deleteById("Student", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteStudentSQL)) {
//            stmt.setInt(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void changeFirstName(int id, String firstName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateStudentFirstNameSQL)) {
            stmt.setString(1, firstName);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void changeMiddleName(int id, String middleName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateStudentMiddleNameSQL)) {
            stmt.setString(1, middleName);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void changeLastName(int id, String lastName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateStudentLastNameSQL)) {
            stmt.setString(1, lastName);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void setGroupId(int studentId, String newGroupId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setGroupIdSQL)) {
            stmt.setString(1, newGroupId);
            stmt.setInt(2, studentId);
            stmt.execute();
            conn.commit();
        }
    }
}
