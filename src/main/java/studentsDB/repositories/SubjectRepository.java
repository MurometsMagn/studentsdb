package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Subject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SubjectRepository {
    private final Connection conn;
    private static final String getAllSubjectsSQL = "SELECT * FROM Subject";
    private static final String setSubjectSQL = "INSERT INTO Subject (name) VALUES(?)";
    private static final String deleteSubjectSQL = "DELETE FROM Subject WHERE id=?";
    private static final String updateSubjectSQL = "UPDATE Subject SET name=? WHERE id=?";
    private static final String setSubjectTypeSQL = "UPDATE Subject SET name=? WHERE id=?";

    public SubjectRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Subject> getAllSubjects() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllSubjectsSQL)) {
                List<Subject> result = new ArrayList<>();
                while (rs.next()) {
                    Subject subject = new Subject(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getInt("subject_type"),
                            (rs.getInt("subject_type") == 0)
                    );
                    result.add(subject);
                }
                return result;
            }
        }
    }

    public void setSubject(String name) throws SQLException {
        if(name.isEmpty()) return;
        try (PreparedStatement stmt = conn.prepareStatement(setSubjectSQL)) {
            stmt.setString(1, name);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteSubject(int id) throws SQLException{
        Database.getInstance().getDeleterById().deleteById("Subject", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteSubjectSQL)) {
//            stmt.setInt(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void editSubject(int id, String name) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement(updateSubjectSQL)) {
            stmt.setString(1, name);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public boolean setSubjectType(int id, Integer type) throws SQLException {
        if (type != 0 && type !=1) return false;
        try (PreparedStatement stmt = conn.prepareStatement(setSubjectTypeSQL)) {
            stmt.setInt(1, type);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
            return true;
        }
    }
}
