package studentsDB.repositories;

import studentsDB.Database;
import studentsDB.models.Teacher;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeacherRepository {
    private final Connection conn;
    private static final String getAllTeachersSQL = "SELECT * FROM Teacher";
    private static final String setTeacherSQL = "INSERT INTO Teacher (first_name, middle_name, last_name) VALUES(?, ?, ?)";
    private static final String deleteTeacherSQL = "DELETE FROM Teacher WHERE id=? ";
    private static final String updateTeacherFirstNameSQL = "UPDATE Teacher SET first_name=? WHERE id=?";
    private static final String updateTeacherMiddleNameSQL = "UPDATE Teacher SET middle_name=? WHERE id=?";
    private static final String updateTeacherLastNameSQL = "UPDATE Teacher SET last_name=? WHERE id=?";
    //private static final String getDepartmentNameById = "SELECT name FROM Department WHERE id=?";
    private static final String setDepartmentIdSQL = "UPDATE Teacher SET department_id=? WHERE id=?";
    private static final String setPositionSQL = "UPDATE Teacher SET position=? WHERE id=?";
    private static final String setScientificDegreeSQL = "UPDATE Teacher SET scientific_degree=? WHERE id=?";

    public TeacherRepository(Connection conn) throws SQLException {
        this.conn = conn;
    }

    public List<Teacher> getAllTeachers() throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(getAllTeachersSQL)){
                List<Teacher> result = new ArrayList<>();
                while (rs.next()) {
                    Teacher teacher = new Teacher(
                            rs.getInt("id"),
                            rs.getString("last_name"),
                            rs.getString("first_name"),
                            rs.getString("middle_name"),
                            rs.getInt("department_id"),
                            //UtilityRepo.getNameById("Department", rs.getInt("department_id")),
                            rs.getString("position"),
                            rs.getString("scientific_degree")
                    );
                    result.add(teacher);
                }
                return result;
            }
        }
    }

//    public  String getDepartmentNameById(Integer department_id) throws SQLException {
//        String departmentName = null;
//        try (PreparedStatement stmt = conn.prepareStatement(getDepartmentNameById)) {
//            stmt.setInt(1, department_id);
//            try (ResultSet rs = stmt.executeQuery()) {
//                departmentName = rs.getString("name");
//            }
//        }
//        return departmentName;
//    }

    public void setTeacher(String firstName, String middleName, String lastName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setTeacherSQL)) {
            stmt.setString(1, firstName);
            stmt.setString(2, middleName);
            stmt.setString(3, lastName);
            stmt.execute();
            conn.commit();
        }
    }

    public void deleteTeacher(int id) throws SQLException{
        Database.getInstance().getDeleterById().deleteById("Teacher", id);
//        try (PreparedStatement stmt = conn.prepareStatement(deleteTeacherSQL)) {
//            stmt.setInt(1, id);
//            stmt.execute();
//            conn.commit();
//        }
    }

    public void changeFirstName(int id, String firstName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateTeacherFirstNameSQL)) {
            stmt.setString(1, firstName);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void changeMiddleName(int id, String middleName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateTeacherMiddleNameSQL)) {
            stmt.setString(1, middleName);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void changeLastName(int id, String lastName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(updateTeacherLastNameSQL)) {
            stmt.setString(1, lastName);
            stmt.setInt(2, id);
            stmt.execute();
            conn.commit();
        }
    }

    public void setDepartmentId(int teacherId, int departmentId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setDepartmentIdSQL)) {
            stmt.setInt(1, departmentId);
            stmt.setInt(2, teacherId);
            stmt.execute();
            conn.commit();
        }
    }

    public void setPosition(int teacherId, String position) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setPositionSQL)) {
            stmt.setString(1, position);
            stmt.setInt(2, teacherId);
            stmt.execute();
            conn.commit();
        }
    }

    public void setScientificDegree(int teacherId, String scientificDegree) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(setScientificDegreeSQL)) {
            stmt.setString(1, scientificDegree);
            stmt.setInt(2, teacherId);
            stmt.execute();
            conn.commit();
        }
    }
}
